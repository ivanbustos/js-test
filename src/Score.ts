type ScoreDefinition = {
  x: number,
  y: number
}

export class Score {

  private scores: ScoreDefinition[] = [];

  constructor(scores: string[]) {
    scores.forEach((v, i) => {
      const matches = v.match(/^(\d+):(\d+)$/)
      if (!matches) {
        throw new Error(`String at position ${i} must have the shape \d+:\d+.`)
      }
      this.scores.push({
        x: parseInt(matches[1]),
        y: parseInt(matches[2])
      })
    })
  }

  public countScore(): Number {
    return this.scores.reduce((accumulator, currentValue) => {
      if (currentValue.x > currentValue.y) {
        return accumulator + 3
      }
      if (currentValue.x === currentValue.y) {
        return accumulator + 1
      }
      return accumulator
    }, 0)
  }

}
