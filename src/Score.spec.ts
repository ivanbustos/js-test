import {Score} from "./Score";

describe('The Score class', () => {
  it('Throws an error when initialized with wrong parameters', () => {
    expect.assertions(1)
    try {
      new Score(['42:12', 'd:d'])
    } catch (e) {
      expect(e).toHaveProperty('message', 'String at position 1 must have the shape d+:d+.')
    }
  })
  it('Calculates scores appropriately', () => {
    const score: Score = new Score(['3:1', '3:3', '0:1', '1:1'])
    expect(score.countScore()).toEqual(5)
  })
})
