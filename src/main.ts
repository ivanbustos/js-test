import {Score} from "./Score";

if (process.argv[2]) {
  const score = new Score(eval(process.argv[2]))
  console.log(score.countScore())
}
else {
  console.log('Try this app by running npm run start "[\'1:0\', \'1:1\', \'0:1\']"')
}
