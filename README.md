# Installation

Install dependencies by running:

`npm run install`

# Usage

Append the score list string as a command parameter. Example:

Run `npm run start "['1:0', '1:1', '0:1']"`

# Running tests

Run `npm run test`
